const apiKey = '41be6622edc4b9e19b9bfa597be6071a'

const searchResults = async e => {
   
    const searchResults =  document.getElementById("searchResults")
    searchResults.textContent = ""

    if(e.length == 0){
        searchResults.classList.add("hide");
    }
    
    if(e.length > 3){

        searchResults.classList.remove("hide");
        const url = `https://api-adresse.data.gouv.fr/search/?q=${e}&autocomplete=0`
        const data = await fetch(url)
        const res = await data.json()
        res.features.forEach((el, i) => {
            i > 0
            ? (searchResults.innerHTML += '<hr>')
            : (searchResults.innerHTML = '');
            searchResults.innerHTML += `<p class="city">${el.properties.city}</p>`
        })  
        const citys = [...document.querySelectorAll(".city")]
        citys.map(ele => ele.addEventListener('click', function() {
            const e = document.getElementById('search').value = ele.innerHTML
            renderWeather(e)
            searchResults.classList.add("hide");
        }))

    }
    
}

//CONNECT TO WEATHER API
const renderWeather = async (e) => {
    
    const url = "https://api.openweathermap.org/data/2.5/weather?q=" + e + "&units=metric&appid=" + apiKey
    await fetch(url)
    .then(data => data.json())
    .then (data => {
        //City
        document.getElementById("city_name").textContent = data.name
        //Humidity
        document.getElementById("humidity").textContent = data.main.humidity
        //Temperature
        document.getElementById("temperature").textContent = data.main.temp
        //Temperature feels like
        document.getElementById("temperature_feels_like").textContent = data.main.feels_like
        //Temp min
        document.getElementById("temperature_min").textContent = data.main.temp_min
        //Temp max
        document.getElementById("temperature_max").textContent = data.main.temp_max
        //Wind
        document.getElementById("wind_speed").textContent = data.wind.speed
    })
    
}

var locationDiv = document.getElementById("location");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(getPosition);
  } else { 
    locationDiv.innerHTML = "Geolocation is not supported by this browser.";
  }
}

const getPosition = async (position) => {
    const latitude = position.coords.latitude
    const longitude = position.coords.longitude
    const url = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${apiKey}`
    await fetch(url)
    .then(data => data.json())
    .then(data =>{
        //City
        document.getElementById("city_name").textContent = data.name
        //Humidity
        document.getElementById("humidity").textContent = data.main.humidity
        //Temperature
        document.getElementById("temperature").textContent = data.main.temp
        //Temperature feels like
        document.getElementById("temperature_feels_like").textContent = data.main.feels_like
        //Temp min
        document.getElementById("temperature_min").textContent = data.main.temp_min
        //Temp max
        document.getElementById("temperature_max").textContent = data.main.temp_max
        //Wind
        document.getElementById("wind_speed").textContent = data.wind.speed
    })
}